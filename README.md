# temp_monitor

temperature monitoring and automatic cooling

## equipment

- something to monitor temperature and cooling it
- raspberry pi
- temp sensor
- relay for switching on the fan
- fan

## requirement

- some db
- aplication to send information about actual temp of sensor
- web interface to see actual temperature and state of fan 
- on/off automatic cooling
- set temperature to on/off fan

## additional

- connect it to grafana
- set notification on slack
